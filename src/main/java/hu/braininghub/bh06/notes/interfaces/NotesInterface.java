package hu.braininghub.bh06.notes.interfaces;

import java.util.List;

import hu.braininghub.bh06.notes.Notes;


public interface NotesInterface {
	
	Notes getNotesByText(String text);
	void saveNotes(Notes notes);
	void deleteNotes(Integer id);
	Notes getNotesById(Integer id);
	List<Notes> getNotes();

}
